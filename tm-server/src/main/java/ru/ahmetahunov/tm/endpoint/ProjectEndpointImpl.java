package ru.ahmetahunov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.endpoint.ProjectEndpoint;
import ru.ahmetahunov.tm.api.service.ServiceLocator;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.Session;
import ru.ahmetahunov.tm.exception.AccessForbiddenException;
import ru.ahmetahunov.tm.exception.InterruptOperationException;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.sql.SQLException;
import java.util.List;

@WebService(endpointInterface = "ru.ahmetahunov.tm.api.endpoint.ProjectEndpoint")
public final class ProjectEndpointImpl implements ProjectEndpoint {

	private ServiceLocator serviceLocator;

	public ProjectEndpointImpl() {}

	public ProjectEndpointImpl(@NotNull final ServiceLocator serviceLocator) {
		this.serviceLocator = serviceLocator;
	}

	@Nullable
	@Override
	@WebMethod
	public Project createProject(
			@WebParam(name = "session") final Session session,
			@WebParam(name = "project") final Project project
	) throws AccessForbiddenException, InterruptOperationException {
		try {
			serviceLocator.getSessionService().validate(session);
			project.setUserId(session.getUserId());
			return serviceLocator.getProjectService().persist(project);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new InterruptOperationException("Operation failed");
		}
	}

	@Nullable
	@Override
	@WebMethod
	public Project updateProject(
			@WebParam(name = "session") final Session session,
			@WebParam(name = "project") final Project project
	) throws AccessForbiddenException, InterruptOperationException {
		try {
			serviceLocator.getSessionService().validate(session);
			project.setUserId(session.getUserId());
			return serviceLocator.getProjectService().merge(project);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new InterruptOperationException("Operation failed");
		}
	}

	@Nullable
	@Override
	@WebMethod
	public Project findOneProject(
			@WebParam(name = "session") final Session session,
			@WebParam(name = "projectId") final String projectId
	) throws AccessForbiddenException, InterruptOperationException {
		try {
			serviceLocator.getSessionService().validate(session);
			return serviceLocator.getProjectService().findOne(session.getUserId(), projectId);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new InterruptOperationException("Operation failed");
		}
	}

	@NotNull
	@Override
	@WebMethod
	public List<Project> findProjectByName(
			@WebParam(name = "session") final Session session,
			@WebParam(name = "projectName") final String projectName
	) throws AccessForbiddenException, InterruptOperationException {
		try {
			serviceLocator.getSessionService().validate(session);
			return serviceLocator.getProjectService().findByName(session.getUserId(), projectName);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new InterruptOperationException("Operation failed");
		}
	}

	@NotNull
	@Override
	@WebMethod
	public List<Project> findProjectByDescription(
			@WebParam(name = "session") final Session session,
			@WebParam(name = "description") final String description
	) throws AccessForbiddenException, InterruptOperationException {
		try {
			serviceLocator.getSessionService().validate(session);
			return serviceLocator.getProjectService().findByDescription(session.getUserId(), description);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new InterruptOperationException("Operation failed");
		}
	}

	@NotNull
	@Override
	@WebMethod
	public List<Project> findProjectByNameOrDesc(
			@WebParam(name = "session") final Session session,
			@WebParam(name = "searchPhrase") final String searchPhrase
	) throws AccessForbiddenException, InterruptOperationException {
		try {
			serviceLocator.getSessionService().validate(session);
			return serviceLocator.getProjectService().findByNameOrDesc(session.getUserId(), searchPhrase);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new InterruptOperationException("Operation failed");
		}
	}

	@NotNull
	@Override
	@WebMethod
	public List<Project> findAllProjects(
			@WebParam(name = "session") final Session session,
			@WebParam(name = "comparatorName") final String comparatorName
	) throws AccessForbiddenException, InterruptOperationException {
		try {
			serviceLocator.getSessionService().validate(session);
			return serviceLocator.getProjectService().findAll(session.getUserId(), comparatorName);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new InterruptOperationException("Operation failed");
		}
	}

	@Override
	@WebMethod
	public void removeAllProjects(
			@WebParam(name = "session") final Session session
	) throws AccessForbiddenException, InterruptOperationException {
		try {
			serviceLocator.getSessionService().validate(session);
			serviceLocator.getProjectService().removeAll(session.getUserId());
		} catch (SQLException e) {
			e.printStackTrace();
			throw new InterruptOperationException("Operation failed");
		}
	}

	@Override
	@WebMethod
	public void removeProject(
			@WebParam(name = "session") final Session session,
			@WebParam(name = "projectId") final String projectId
	) throws AccessForbiddenException, InterruptOperationException {
		try {
			serviceLocator.getSessionService().validate(session);
			serviceLocator.getProjectService().remove(session.getUserId(), projectId);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new InterruptOperationException("Operation failed");
		}
	}

}
