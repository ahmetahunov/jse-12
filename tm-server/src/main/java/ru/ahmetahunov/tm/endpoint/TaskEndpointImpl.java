package ru.ahmetahunov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.endpoint.TaskEndpoint;
import ru.ahmetahunov.tm.api.service.ServiceLocator;
import ru.ahmetahunov.tm.entity.Session;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.exception.AccessForbiddenException;
import ru.ahmetahunov.tm.exception.InterruptOperationException;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.sql.SQLException;
import java.util.List;

@WebService(endpointInterface = "ru.ahmetahunov.tm.api.endpoint.TaskEndpoint")
public final class TaskEndpointImpl implements TaskEndpoint {

	private ServiceLocator serviceLocator;

	public TaskEndpointImpl() {}

	public TaskEndpointImpl(@NotNull final ServiceLocator serviceLocator) {
		this.serviceLocator = serviceLocator;
	}

	@Nullable
	@Override
	@WebMethod
	public Task createTask(
			@WebParam(name = "session") final Session session,
			@WebParam(name = "task") final Task task
	) throws AccessForbiddenException, InterruptOperationException {
		try {
			serviceLocator.getSessionService().validate(session);
			task.setUserId(session.getUserId());
			return serviceLocator.getTaskService().persist(task);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new InterruptOperationException("Operation failed");
		}
	}

	@Nullable
	@Override
	@WebMethod
	public Task updateTask(
			@WebParam(name = "session") final Session session,
			@WebParam(name = "task") final Task task
	) throws AccessForbiddenException, InterruptOperationException {
		try {
			serviceLocator.getSessionService().validate(session);
			task.setUserId(session.getUserId());
			return serviceLocator.getTaskService().merge(task);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new InterruptOperationException("Operation failed");
		}
	}

	@NotNull
	@Override
	@WebMethod
	public List<Task> findAllTasks(
			@WebParam(name = "session") final Session session,
			@WebParam(name = "comparatorName") final String comparatorName
	) throws AccessForbiddenException, InterruptOperationException {
		try {
			serviceLocator.getSessionService().validate(session);
			return serviceLocator.getTaskService().findAll(session.getUserId(), comparatorName);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new InterruptOperationException("Operation failed");
		}
	}

	@NotNull
	@Override
	@WebMethod
	public List<Task> findAllProjectTasks(
			@WebParam(name = "session") final Session session,
			@WebParam(name = "projectId") final String projectId
	) throws AccessForbiddenException, InterruptOperationException {
		try {
			serviceLocator.getSessionService().validate(session);
			return serviceLocator.getTaskService().findAll(session.getUserId(), projectId, "name");
		} catch (SQLException e) {
			e.printStackTrace();
			throw new InterruptOperationException("Operation failed");
		}
	}

	@Nullable
	@Override
	@WebMethod
	public Task findOneTask(
			@WebParam(name = "session") final Session session,
			@WebParam(name = "taskId") final String taskId
	) throws AccessForbiddenException, InterruptOperationException {
		try {
			serviceLocator.getSessionService().validate(session);
			return serviceLocator.getTaskService().findOne(session.getUserId(), taskId);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new InterruptOperationException("Operation failed");
		}
	}

	@NotNull
	@Override
	@WebMethod
	public List<Task> findTaskByName(
			@WebParam(name = "session") final Session session,
			@WebParam(name = "taskName") final String taskName
	) throws AccessForbiddenException, InterruptOperationException {
		try {
			serviceLocator.getSessionService().validate(session);
			return serviceLocator.getTaskService().findByName(session.getUserId(), taskName);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new InterruptOperationException("Operation failed");
		}
	}

	@NotNull
	@Override
	@WebMethod
	public List<Task> findTaskByDescription(
			@WebParam(name = "session") final Session session,
			@WebParam(name = "description") final String description
	) throws AccessForbiddenException, InterruptOperationException {
		try {
			serviceLocator.getSessionService().validate(session);
			return serviceLocator.getTaskService().findByDescription(session.getUserId(), description);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new InterruptOperationException("Operation failed");
		}
	}

	@NotNull
	@Override
	@WebMethod
	public List<Task> findTaskByNameOrDesc(
			@WebParam(name = "session") final Session session,
			@WebParam(name = "searchPhrase") final String searchPhrase
	) throws AccessForbiddenException, InterruptOperationException {
		try {
			serviceLocator.getSessionService().validate(session);
			return serviceLocator.getTaskService().findByNameOrDesc(session.getUserId(), searchPhrase);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new InterruptOperationException("Operation failed");
		}
	}

	@Override
	@WebMethod
	public void removeAllTasks(
			@WebParam(name = "session") final Session session
	) throws AccessForbiddenException, InterruptOperationException {
		try {
			serviceLocator.getSessionService().validate(session);
			serviceLocator.getTaskService().removeAll(session.getUserId());
		} catch (SQLException e) {
			e.printStackTrace();
			throw new InterruptOperationException("Operation failed");
		}
	}

	@Override
	@WebMethod
	public void removeTask(
			@WebParam(name = "session") final Session session,
			@WebParam(name = "taskId") final String taskId
	) throws AccessForbiddenException, InterruptOperationException {
		try {
			serviceLocator.getSessionService().validate(session);
			serviceLocator.getTaskService().remove(session.getUserId(), taskId);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new InterruptOperationException("Operation failed");
		}
	}

}
