package ru.ahmetahunov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.endpoint.AdminEndpoint;
import ru.ahmetahunov.tm.api.service.ServiceLocator;
import ru.ahmetahunov.tm.entity.Session;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.exception.AccessForbiddenException;
import ru.ahmetahunov.tm.exception.InterruptOperationException;
import ru.ahmetahunov.tm.util.PassUtil;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebService(endpointInterface = "ru.ahmetahunov.tm.api.endpoint.AdminEndpoint")
public final class AdminEndpointImpl implements AdminEndpoint {

	private ServiceLocator serviceLocator;

	public AdminEndpointImpl() {}

	public AdminEndpointImpl(@NotNull final ServiceLocator serviceLocator) {
		this.serviceLocator = serviceLocator;
	}

	@Override
	@WebMethod
	public void dataSaveBin(
			@WebParam(name = "session") final Session session
	) throws InterruptOperationException, AccessForbiddenException, IOException {
		try {
			serviceLocator.getSessionService().validate(session, Role.ADMINISTRATOR);
			serviceLocator.getDataService().dataSaveBin();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new InterruptOperationException("Operation failed");
		}
	}

	@Override
	@WebMethod
	public void dataSaveXmlJaxb(
			@WebParam(name = "session") final Session session
	) throws AccessForbiddenException, IOException, InterruptOperationException {
		try {
			serviceLocator.getSessionService().validate(session, Role.ADMINISTRATOR);
			serviceLocator.getDataService().dataSaveXmlJaxb();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new InterruptOperationException("Operation failed");
		}
	}

	@Override
	@WebMethod
	public void dataSaveXmlJackson(
			@WebParam(name = "session") final Session session
	) throws AccessForbiddenException, IOException, InterruptOperationException {
		try {
			serviceLocator.getSessionService().validate(session, Role.ADMINISTRATOR);
			serviceLocator.getDataService().dataSaveXmlJackson();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new InterruptOperationException("Operation failed");
		}
	}

	@Override
	@WebMethod
	public void dataSaveJsonJaxb(
			@WebParam(name = "session") final Session session
	) throws AccessForbiddenException, IOException, InterruptOperationException {
		try {
			serviceLocator.getSessionService().validate(session, Role.ADMINISTRATOR);
			serviceLocator.getDataService().dataSaveJsonJaxb();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new InterruptOperationException("Operation failed");
		}
	}

	@Override
	@WebMethod
	public void dataSaveJsonJackson(
			@WebParam(name = "session") final Session session
	) throws AccessForbiddenException, IOException, InterruptOperationException {
		try {
			serviceLocator.getSessionService().validate(session, Role.ADMINISTRATOR);
			serviceLocator.getDataService().dataSaveJsonJackson();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new InterruptOperationException("Operation failed");
		}
	}

	@Override
	@WebMethod
	public void dataLoadBin(
			@WebParam(name = "session") final Session session
	) throws AccessForbiddenException, InterruptOperationException, IOException, ClassNotFoundException {
		try {
			serviceLocator.getSessionService().validate(session, Role.ADMINISTRATOR);
			serviceLocator.getDataService().dataLoadBin();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new InterruptOperationException("Operation failed");
		}
	}

	@Override
	@WebMethod
	public void dataLoadXmlJaxb(
			@WebParam(name = "session") final Session session
	) throws AccessForbiddenException, InterruptOperationException, IOException {
		try {
			serviceLocator.getSessionService().validate(session, Role.ADMINISTRATOR);
			serviceLocator.getDataService().dataLoadXmlJaxb();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new InterruptOperationException("Operation failed");
		}
	}

	@Override
	@WebMethod
	public void dataLoadXmlJackson(
			@WebParam(name = "session") final Session session
	) throws AccessForbiddenException, IOException, InterruptOperationException {
		try {
			serviceLocator.getSessionService().validate(session, Role.ADMINISTRATOR);
			serviceLocator.getDataService().dataLoadXmlJackson();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new InterruptOperationException("Operation failed");
		}
	}

	@Override
	@WebMethod
	public void dataLoadJsonJaxb(
			@WebParam(name = "session") final Session session
	) throws AccessForbiddenException, InterruptOperationException {
		try {
			serviceLocator.getSessionService().validate(session, Role.ADMINISTRATOR);
			serviceLocator.getDataService().dataLoadJsonJaxb();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new InterruptOperationException("Operation failed");
		}
	}

	@Override
	@WebMethod
	public void dataLoadJsonJackson(
			@WebParam(name = "session") final Session session
	) throws AccessForbiddenException, InterruptOperationException, IOException {
		try {
			serviceLocator.getSessionService().validate(session, Role.ADMINISTRATOR);
			serviceLocator.getDataService().dataLoadJsonJackson();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new InterruptOperationException("Operation failed");
		}
	}

	@NotNull
	@Override
	@WebMethod
	public List<User> findAllUsers(
			@WebParam(name = "session") final Session session
	) throws AccessForbiddenException, InterruptOperationException {
		try {
			serviceLocator.getSessionService().validate(session, Role.ADMINISTRATOR);
			return serviceLocator.getUserService().findAll();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new InterruptOperationException("Operation failed");
		}
	}

	@Override
	@WebMethod
	public void userUpdatePasswordAdmin(
			@WebParam(name = "session") final Session session,
			@WebParam(name = "userId") final String userId,
			@WebParam(name = "password") final String password
	) throws AccessForbiddenException, InterruptOperationException {
		try {
			serviceLocator.getSessionService().validate(session, Role.ADMINISTRATOR);
			serviceLocator.getUserService().updatePasswordAdmin(userId, password);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new InterruptOperationException("Operation failed");
		}
	}

	@Override
	@WebMethod
	public void userChangeRoleAdmin(
			@WebParam(name = "session") final Session session,
			@WebParam(name = "userId") final String userId,
			@WebParam(name = "role") final Role role
	) throws AccessForbiddenException, InterruptOperationException {
		try {
			serviceLocator.getSessionService().validate(session, Role.ADMINISTRATOR);
			serviceLocator.getUserService().updateRole(userId, role);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new InterruptOperationException("Operation failed");
		}
	}

	@Override
	@WebMethod
	public void userRemove(
			@WebParam(name = "session") final Session session,
			@WebParam(name = "userId") final String userId
	) throws AccessForbiddenException, InterruptOperationException {
		try {
			serviceLocator.getSessionService().validate(session, Role.ADMINISTRATOR);
			serviceLocator.getUserService().remove(userId);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new InterruptOperationException("Operation failed");
		}
	}

	@Nullable
	@Override
	@WebMethod
	public User userRegisterAdmin(
			@WebParam(name = "session") final Session session,
			@WebParam(name = "user") final String login,
			@WebParam(name = "password") final String password
	) throws AccessForbiddenException, InterruptOperationException {
		try {
			serviceLocator.getSessionService().validate(session, Role.ADMINISTRATOR);
			if (serviceLocator.getUserService().contains(login))
				throw new InterruptOperationException("This login already exists!");
			@NotNull final User user = new User();
			user.setLogin(login);
			user.setPassword(PassUtil.getHash(password));
			return serviceLocator.getUserService().persist(user);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new InterruptOperationException("Operation failed");
		}
	}

	@Nullable
	@Override
	@WebMethod
	public User userFindByLogin(
			@WebParam(name = "session") final Session session,
			@WebParam(name = "login") final String login
	) throws AccessForbiddenException, InterruptOperationException {
		try {
			serviceLocator.getSessionService().validate(session, Role.ADMINISTRATOR);
			return serviceLocator.getUserService().findUser(login);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new InterruptOperationException("Operation failed");
		}
	}

}
