package ru.ahmetahunov.tm.exception;

import org.jetbrains.annotations.Nullable;

public final class AccessForbiddenException extends Exception {

	public AccessForbiddenException() {}

	public AccessForbiddenException(@Nullable final String message) {
		super(message);
	}

}
