package ru.ahmetahunov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.entity.Session;
import java.sql.SQLException;

public interface ISessionRepository extends IRepository<Session> {

	@Nullable
	public Session findOne(@NotNull String id, @NotNull String userId) throws SQLException;

	public void remove(@NotNull String id, @NotNull String userId) throws SQLException;

}
