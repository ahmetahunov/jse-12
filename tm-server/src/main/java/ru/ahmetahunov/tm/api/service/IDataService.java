package ru.ahmetahunov.tm.api.service;

import ru.ahmetahunov.tm.exception.InterruptOperationException;
import java.io.IOException;
import java.sql.SQLException;

public interface IDataService {

	public void dataSaveBin() throws IOException, SQLException, InterruptOperationException;

	public void dataSaveXmlJaxb() throws IOException, InterruptOperationException;

	public void dataSaveXmlJackson() throws IOException, SQLException, InterruptOperationException;

	public void dataSaveJsonJaxb() throws IOException, InterruptOperationException;

	public void dataSaveJsonJackson() throws IOException, SQLException, InterruptOperationException;

	public void dataLoadBin() throws InterruptOperationException, IOException, ClassNotFoundException, SQLException;

	public void dataLoadXmlJaxb() throws InterruptOperationException, IOException;

	public void dataLoadXmlJackson() throws InterruptOperationException, IOException, SQLException;

	public void dataLoadJsonJaxb() throws InterruptOperationException;

	public void dataLoadJsonJackson() throws InterruptOperationException, IOException, SQLException;

}
