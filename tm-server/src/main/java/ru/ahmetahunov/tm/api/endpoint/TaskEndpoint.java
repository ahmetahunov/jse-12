package ru.ahmetahunov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.entity.Session;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.exception.AccessForbiddenException;
import ru.ahmetahunov.tm.exception.InterruptOperationException;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface TaskEndpoint {

	@Nullable
	@WebMethod
	public Task createTask(
			@WebParam(name = "session") Session session,
			@WebParam(name = "task") Task task
	) throws AccessForbiddenException, InterruptOperationException;

	@Nullable
	@WebMethod
	public Task updateTask(
			@WebParam(name = "session") Session session,
			@WebParam(name = "task") Task task
	) throws AccessForbiddenException, InterruptOperationException;

	@NotNull
	@WebMethod
	public List<Task> findAllTasks(
			@WebParam(name = "session") Session session,
			@WebParam(name = "comparator") String comparator
	) throws AccessForbiddenException, InterruptOperationException;

	@NotNull
	@WebMethod
	public List<Task> findAllProjectTasks(
			@WebParam(name = "session") Session session,
			@WebParam(name = "projectId") String projectId
	) throws AccessForbiddenException, InterruptOperationException;

	@Nullable
	@WebMethod
	public Task findOneTask(
			@WebParam(name = "session") Session session,
			@WebParam(name = "taskId") String taskId
	) throws AccessForbiddenException, InterruptOperationException;

	@NotNull
	@WebMethod
	public List<Task> findTaskByName(
			@WebParam(name = "session") Session session,
			@WebParam(name = "taskName") String taskName
	) throws AccessForbiddenException, InterruptOperationException;

	@NotNull
	@WebMethod
	public List<Task> findTaskByDescription(
			@WebParam(name = "session") Session session,
			@WebParam(name = "description") String description
	) throws AccessForbiddenException, InterruptOperationException;

	@NotNull
	@WebMethod
	public List<Task> findTaskByNameOrDesc(
			@WebParam(name = "session") Session session,
			@WebParam(name = "searchPhrase") String searchPhrase
	) throws AccessForbiddenException, InterruptOperationException;

	@WebMethod
	public void removeAllTasks(
			@WebParam(name = "session") Session session
	) throws AccessForbiddenException, InterruptOperationException;

	@WebMethod
	public void removeTask(
			@WebParam(name = "session") Session session,
			@WebParam(name = "taskId") String taskId
	) throws AccessForbiddenException, InterruptOperationException;

}
