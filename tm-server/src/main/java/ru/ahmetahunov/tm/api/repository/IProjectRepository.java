package ru.ahmetahunov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.entity.Project;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    @Nullable
    public Project findOne(@NotNull String userId, @NotNull String projectName) throws SQLException;

    @NotNull
    public List<Project> findAll(@NotNull String userId) throws SQLException;

    @NotNull
    public List<Project> findAll(@NotNull String userId, @NotNull String comparator) throws SQLException;

    @NotNull
    public List<Project> findByName(@NotNull String userId, @NotNull String projectName) throws SQLException;

    @NotNull
    public List<Project> findByDescription(@NotNull String userId, @NotNull String description) throws SQLException;

    @NotNull
    public List<Project> findByNameOrDesc(@NotNull String userId, @NotNull String searchPhrase) throws SQLException;

    public void removeAll(@NotNull String userId) throws SQLException;

    public void remove(@NotNull String userId, @NotNull String id) throws SQLException;

    public void load(@NotNull Collection<Project> data) throws SQLException;

}
