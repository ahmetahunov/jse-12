package ru.ahmetahunov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.Session;
import ru.ahmetahunov.tm.exception.AccessForbiddenException;
import ru.ahmetahunov.tm.exception.InterruptOperationException;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ProjectEndpoint {

	@Nullable
	@WebMethod
	public Project createProject(
			@WebParam(name = "session") Session session,
			@WebParam(name = "project") Project project
	) throws AccessForbiddenException, InterruptOperationException;

	@Nullable
	@WebMethod
	public Project updateProject(
			@WebParam(name = "session") Session session,
			@WebParam(name = "project") Project project
	) throws AccessForbiddenException, InterruptOperationException;

	@Nullable
	@WebMethod
	public Project findOneProject(
			@WebParam(name = "session") Session session,
			@WebParam(name = "projectId") String projectId
	) throws AccessForbiddenException, InterruptOperationException;

	@NotNull
	@WebMethod
	public List<Project> findProjectByName(
			@WebParam(name = "session") Session session,
			@WebParam(name = "projectName") String projectName
	) throws AccessForbiddenException, InterruptOperationException;

	@NotNull
	@WebMethod
	public List<Project> findProjectByDescription(
			@WebParam(name = "session") Session session,
			@WebParam(name = "description") String description
	) throws AccessForbiddenException, InterruptOperationException;

	@NotNull
	@WebMethod
	public List<Project> findProjectByNameOrDesc(
			@WebParam(name = "session") Session session,
			@WebParam(name = "searchPhrase") String searchPhrase
	) throws AccessForbiddenException, InterruptOperationException;

	@NotNull
	@WebMethod
	public List<Project> findAllProjects(
			@WebParam(name = "session") Session session,
			@WebParam(name = "comparator") String comparator
	) throws AccessForbiddenException, InterruptOperationException;

	@WebMethod
	public void removeAllProjects(
			@WebParam(name = "session") Session session
	) throws AccessForbiddenException, InterruptOperationException;

	@WebMethod
	public void removeProject(
			@WebParam(name = "session") Session session,
			@WebParam(name = "projectId") String projectId
	) throws AccessForbiddenException, InterruptOperationException;

}
