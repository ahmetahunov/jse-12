package ru.ahmetahunov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.entity.AbstractEntity;
import ru.ahmetahunov.tm.exception.InterruptOperationException;
import java.sql.SQLException;
import java.util.List;

public interface IAbstractService<T extends AbstractEntity> {

    @Nullable
    public T persist(T item) throws InterruptOperationException, SQLException;

    @Nullable
    public T merge(T item) throws InterruptOperationException, SQLException;

    @Nullable
    public T findOne(String id) throws InterruptOperationException, SQLException;

    @NotNull
    public List<T> findAll() throws InterruptOperationException, SQLException;

    public void remove(String id) throws SQLException, InterruptOperationException;

    public void remove(T item) throws SQLException, InterruptOperationException;

}
