package ru.ahmetahunov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.exception.InterruptOperationException;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public interface IProjectService extends IAbstractService<Project> {

    @Nullable
    public Project findOne(String userId, String projectId) throws InterruptOperationException, SQLException;

    @NotNull
    public List<Project> findByName(String userId, String projectName) throws InterruptOperationException, SQLException;

    @NotNull
    public List<Project> findByDescription(String userId, String description) throws InterruptOperationException, SQLException;

    @NotNull
    public List<Project> findByNameOrDesc(String userId, String searchPhrase) throws InterruptOperationException, SQLException;

    @NotNull
    public List<Project> findAll(String userId) throws InterruptOperationException, SQLException;

    @NotNull
    public List<Project> findAll(String userId, String comparator) throws InterruptOperationException, SQLException;

    public void removeAll(String userId) throws InterruptOperationException, SQLException;

    public void remove(String userId, String projectId) throws InterruptOperationException, SQLException;

    public void load(Collection<Project> data) throws InterruptOperationException, SQLException;

}
