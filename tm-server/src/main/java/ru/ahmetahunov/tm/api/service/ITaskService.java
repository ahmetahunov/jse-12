package ru.ahmetahunov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.exception.InterruptOperationException;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public interface ITaskService extends IAbstractService<Task> {

    @Nullable
    public Task findOne(String userId, String taskId) throws InterruptOperationException, SQLException;

    @NotNull
    public List<Task> findAll(String userId) throws InterruptOperationException, SQLException;

    @NotNull
    public List<Task> findAll(String userId, String comparator) throws InterruptOperationException, SQLException;

    @NotNull
    public List<Task> findAll(String userId, String projectId, String comparator) throws InterruptOperationException, SQLException;

    @NotNull
    public List<Task> findByName(String userId, String taskName) throws InterruptOperationException, SQLException;

    @NotNull
    public List<Task> findByDescription(String userId, String description) throws InterruptOperationException, SQLException;

    @NotNull
    public List<Task> findByNameOrDesc(String userId, String searchPhrase) throws InterruptOperationException, SQLException;

    public void remove(String userId, String taskId) throws SQLException, InterruptOperationException;

    public void removeAll(String userId) throws InterruptOperationException, SQLException;

    public void load(Collection<Task> data) throws InterruptOperationException, SQLException;

}
