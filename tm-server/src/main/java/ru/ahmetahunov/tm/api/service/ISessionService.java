package ru.ahmetahunov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.entity.Session;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.exception.AccessForbiddenException;
import ru.ahmetahunov.tm.exception.InterruptOperationException;
import java.sql.SQLException;

public interface ISessionService extends IAbstractService<Session> {

	@Nullable
	public Session findOne(String id, String userId) throws InterruptOperationException, SQLException;

	public void remove(String id, String userId) throws InterruptOperationException, SQLException;

	public void validate(Session session) throws AccessForbiddenException, InterruptOperationException, SQLException;

	public void validate(Session session, Role role) throws AccessForbiddenException, SQLException, InterruptOperationException;

}
