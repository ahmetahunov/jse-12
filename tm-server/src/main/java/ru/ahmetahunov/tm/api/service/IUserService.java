package ru.ahmetahunov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.exception.AccessForbiddenException;
import ru.ahmetahunov.tm.exception.InterruptOperationException;
import java.sql.SQLException;
import java.util.Collection;

public interface IUserService extends IAbstractService<User> {

    @Nullable
    public User findUser(String login) throws InterruptOperationException, SQLException;

    public boolean contains(String login) throws SQLException, InterruptOperationException;

    public void updatePasswordAdmin(String userId, String password) throws InterruptOperationException, AccessForbiddenException, SQLException;

    public void updateRole(String userId, Role role) throws InterruptOperationException, SQLException;

    public void updatePassword(String userId, String oldPassword, String newPassword) throws AccessForbiddenException, InterruptOperationException, SQLException;

    public void updateLogin(String userId, String login) throws AccessForbiddenException, InterruptOperationException, SQLException;

    public void load(Collection<User> data) throws InterruptOperationException, SQLException;

}
