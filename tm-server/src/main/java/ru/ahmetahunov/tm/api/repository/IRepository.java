package ru.ahmetahunov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.entity.AbstractEntity;
import java.sql.SQLException;
import java.util.List;

public interface IRepository<T extends AbstractEntity> {

    @Nullable
    public T persist(@NotNull T item) throws SQLException;

    @Nullable
    public T merge(@NotNull T item) throws SQLException;

    @Nullable
    public T findOne(@NotNull String id) throws SQLException;

    @NotNull
    public List<T> findAll() throws SQLException;

    public void remove(@NotNull String id) throws SQLException;

}
