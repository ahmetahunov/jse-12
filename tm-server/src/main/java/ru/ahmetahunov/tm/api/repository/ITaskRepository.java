package ru.ahmetahunov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.entity.Task;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    @NotNull
    public List<Task> findAll(@NotNull String userId) throws SQLException;

    @NotNull
    public List<Task> findAll(@NotNull String userId, @NotNull String comparator) throws SQLException;

    @NotNull
    public List<Task> findAll(@NotNull String userId, @NotNull String projectId, @NotNull String comparator) throws SQLException;

    @Nullable
    public Task findOne(@NotNull String userId, @NotNull String taskId) throws SQLException;

    @NotNull
    public List<Task> findByName(@NotNull String userId, @NotNull String taskName) throws SQLException;

    @NotNull
    public List<Task> findByNameOrDesc(@NotNull String userId, @NotNull String searchPhrase) throws SQLException;

    @NotNull
    public List<Task> findByDescription(@NotNull String userId, @NotNull String description) throws SQLException;

    public void remove(@NotNull String userId, @NotNull String taskId) throws SQLException;

    public void removeAll(@NotNull String userId) throws SQLException;

    public void load(@NotNull Collection<Task> data) throws SQLException;

}
