package ru.ahmetahunov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.entity.User;
import java.sql.SQLException;
import java.util.Collection;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    public User findUser(@NotNull String login) throws SQLException;

    public void removeAll() throws SQLException;

    public void load(@NotNull Collection<User> data) throws SQLException;

    public boolean contains(@NotNull String login) throws SQLException;

}
