package ru.ahmetahunov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.entity.Property;
import ru.ahmetahunov.tm.exception.IdCollisionException;
import ru.ahmetahunov.tm.exception.InterruptOperationException;

public interface IPropertyService {

	public void persist(Property property) throws IdCollisionException, InterruptOperationException;

	public void merge(Property property) throws InterruptOperationException;

	@NotNull
	public Property findOne(String propertyName) throws InterruptOperationException;

	@Nullable
	public Property remove(String propertyName) throws InterruptOperationException;

	public void clear();

}
