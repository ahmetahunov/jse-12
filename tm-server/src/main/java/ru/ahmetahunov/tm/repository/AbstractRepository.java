package ru.ahmetahunov.tm.repository;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.repository.IRepository;
import ru.ahmetahunov.tm.entity.AbstractEntity;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;

@RequiredArgsConstructor
public abstract class AbstractRepository<T extends AbstractEntity> implements IRepository<T> {

    @NotNull
    protected final Connection connection;

    @NotNull
    @Override
    public abstract T persist(@NotNull final T item) throws SQLException;

    @NotNull
    @Override
    public abstract T merge(@NotNull final T item) throws SQLException;

    @Nullable
    @Override
    public abstract T findOne(@NotNull final String id) throws SQLException;

    @NotNull
    @Override
    public abstract List<T> findAll() throws SQLException;

    @Override
    public abstract void remove(@NotNull final String id) throws SQLException;

}
