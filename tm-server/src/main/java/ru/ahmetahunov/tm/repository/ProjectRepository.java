package ru.ahmetahunov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.repository.IProjectRepository;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.enumerated.Status;
import ru.ahmetahunov.tm.util.DateUtil;
import java.sql.*;
import java.util.*;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    public Project persist(@NotNull final Project project) throws SQLException {
        @NotNull final String query = "INSERT INTO app_project VALUES (?, ?, ?, ?, ?, NOW(), ?, ?)";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, project.getId());
            statement.setString(2, project.getName());
            statement.setString(3, project.getDescription());
            statement.setDate(4, DateUtil.parseDate(project.getStartDate()));
            statement.setDate(5, DateUtil.parseDate(project.getFinishDate()));
            statement.setString(6, project.getStatus().toString());
            statement.setString(7, project.getUserId());
            statement.executeUpdate();
            return project;
        }
    }

    @NotNull
    @Override
    public Project merge(@NotNull final Project project) throws SQLException {
        @NotNull final String query = "UPDATE app_project SET name = ? , description = ?," +
                " startDate = ?, finishDate = ?, status = ? WHERE user_id = ? AND id = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, project.getName());
            statement.setString(2, project.getDescription());
            statement.setDate(3, DateUtil.parseDate(project.getStartDate()));
            statement.setDate(4, DateUtil.parseDate(project.getFinishDate()));
            statement.setString(5, project.getStatus().toString());
            statement.setString(6, project.getUserId());
            statement.setString(7, project.getId());
            statement.executeUpdate();
            return project;
        }
    }

    @Nullable
    @Override
    public Project findOne(@NotNull final String id) throws SQLException {
        @NotNull final String query = "SELECT * FROM app_project WHERE id = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, id);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            @Nullable Project project = null;
            if (resultSet.first()) project = transformResult(resultSet);
            resultSet.close();
            return project;
        }
    }

    @Nullable
    @Override
    public Project findOne(
            @NotNull final String userId,
            @NotNull final String projectId
    ) throws SQLException {
        @NotNull final String query = "SELECT * FROM app_project WHERE user_id = ? AND id = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, userId);
            statement.setString(2, projectId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            @Nullable Project project = null;
            if (resultSet.first()) project = transformResult(resultSet);
            resultSet.close();
            return project;
        }
    }

    @NotNull
    @Override
    public List<Project> findAll() throws SQLException {
        @NotNull final String query = "SELECT * FROM app_project";
        try (@NotNull final Statement statement = connection.createStatement();
             @NotNull final ResultSet resultSet = statement.executeQuery(query)) {
            @NotNull final List<Project> projects = new ArrayList<>();
            while (resultSet.next()) projects.add(transformResult(resultSet));
            return projects;
        }
    }

    @NotNull
    @Override
    public List<Project> findAll(
            @NotNull final String userId,
            @NotNull final String comparator
    ) throws SQLException {
        @NotNull final String query = "SELECT * FROM app_project WHERE user_id = ? ORDER BY " + comparator;
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, userId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            @NotNull final List<Project> projects = new ArrayList<>();
            while (resultSet.next()) projects.add(transformResult(resultSet));
            resultSet.close();
            return projects;
        }
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull final String userId) throws SQLException {
        return findAll(userId, "name");
    }

    @NotNull
    @Override
    public List<Project> findByName(
            @NotNull final String userId,
            @NotNull final String projectName
    ) throws SQLException {
        @NotNull final String query = "SELECT * FROM app_project WHERE user_id = ? " +
                "AND name LIKE ? ORDER BY name";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, userId);
            statement.setString(2, "%" + projectName + "%");
            @NotNull final ResultSet resultSet = statement.executeQuery();
            @NotNull final List<Project> projects = new ArrayList<>();
            while (resultSet.next()) projects.add(transformResult(resultSet));
            resultSet.close();
            return projects;
        }
    }

    @NotNull
    @Override
    public List<Project> findByDescription(
            @NotNull final String userId,
            @NotNull final String description
    ) throws SQLException {
        @NotNull final String query = "SELECT * FROM app_project WHERE user_id = ?" +
                " AND description LIKE ? ORDER BY description";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, userId);
            statement.setString(2, "%" + description + "%");
            @NotNull final ResultSet resultSet = statement.executeQuery();
            @NotNull final List<Project> projects = new ArrayList<>();
            while (resultSet.next()) projects.add(transformResult(resultSet));
            resultSet.close();
            return projects;
        }
    }

    @NotNull
    @Override
    public List<Project> findByNameOrDesc(
            @NotNull final String userId,
            @NotNull final String searchPhrase
    ) throws SQLException {
        @NotNull final String query = "SELECT * FROM app_project WHERE user_id = ? " +
                "AND name LIKE ? OR description LIKE ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, userId);
            statement.setString(2, "%" + searchPhrase + "%");
            statement.setString(3, "%" + searchPhrase + "%");
            @NotNull final ResultSet resultSet = statement.executeQuery();
            @NotNull final List<Project> projects = new ArrayList<>();
            while (resultSet.next()) projects.add(transformResult(resultSet));
            resultSet.close();
            return projects;
        }
    }

    @Override
    public void remove(@NotNull final String id) throws SQLException {
        @NotNull final String query = "DELETE FROM app_project WHERE id = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, id);
            statement.executeUpdate();
        }
    }

    @Override
    public void remove(
            @NotNull final String userId,
            @NotNull final String id
    ) throws SQLException {
        @NotNull final String query = "DELETE FROM app_project WHERE user_id = ? AND id = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, userId);
            statement.setString(2, id);
            statement.executeUpdate();
        }
    }

    @Override
    public void removeAll(@NotNull final String userId) throws SQLException {
        @NotNull final String query = "DELETE FROM app_project WHERE user_id = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, userId);
            statement.executeUpdate();
        }
    }

    @Override
    public void load(@NotNull final Collection<Project> data) throws SQLException {
        @NotNull final String query = "INSERT INTO app_project VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            for (@NotNull final Project project : data) {
                statement.setString(1, project.getId());
                statement.setString(2, project.getName());
                statement.setString(3, project.getDescription());
                statement.setDate(4, DateUtil.parseDate(project.getStartDate()));
                statement.setDate(5, DateUtil.parseDate(project.getFinishDate()));
                statement.setTimestamp(6, DateUtil.getTimestamp(project.getCreationDate()));
                statement.setString(7, project.getStatus().toString());
                statement.setString(8, project.getUserId());
                statement.addBatch();
            }
            statement.executeBatch();
            statement.clearBatch();
        }
    }

    @NotNull
    private Project transformResult(@NotNull final ResultSet resultSet) throws SQLException {
        @NotNull final Project project = new Project();
        project.setId(resultSet.getString("id"));
        project.setName(resultSet.getString("name"));
        project.setStatus(Status.valueOf(resultSet.getString("status")));
        project.setDescription(resultSet.getString("description"));
        project.setCreationDate(resultSet.getDate("creationDate"));
        project.setStartDate(resultSet.getDate("startDate"));
        project.setFinishDate(resultSet.getDate("finishDate"));
        project.setUserId(resultSet.getString("user_id"));
        return project;
    }

}
