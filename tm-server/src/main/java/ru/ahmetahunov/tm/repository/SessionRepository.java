package ru.ahmetahunov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.repository.ISessionRepository;
import ru.ahmetahunov.tm.entity.Session;
import ru.ahmetahunov.tm.enumerated.Role;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public final class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

	public SessionRepository(@NotNull final Connection connection) {
		super(connection);
	}

	@NotNull
	@Override
	public Session persist(@NotNull final Session session) throws SQLException {
		@NotNull final String query = "INSERT INTO app_session VALUES (?, ?, ?, ?, ?)";
		try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
			statement.setString(1, session.getId());
			statement.setString(2, session.getUserId());
			statement.setString(3, session.getSignature());
			statement.setString(4, session.getRole().toString());
			statement.setLong(5, session.getTimestamp());
			statement.executeUpdate();
			return session;
		}
	}

	@NotNull
	@Override
	public Session merge(@NotNull final Session session) throws SQLException {
		@NotNull final String query = "UPDATE app_session SET user_id = ?, signature = ?," +
				" role = ?, timestamp = ? WHERE id = ?";
		try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
			statement.setString(1, session.getUserId());
			statement.setString(2, session.getSignature());
			statement.setString(3, session.getRole().toString());
			statement.setLong(4, session.getTimestamp());
			statement.setString(5, session.getId());
			statement.executeUpdate();
			return session;
		}
	}

	@Nullable
	@Override
	public Session findOne(@NotNull final String id) throws SQLException {
		@NotNull final String query = "SELECT * FROM app_session WHERE id = ?";
		try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
			statement.setString(1, id);
			@NotNull final ResultSet resultSet = statement.executeQuery();
			@Nullable Session session = null;
			if (resultSet.first()) session = transformResult(resultSet);
			resultSet.close();
			return session;
		}
	}

	@Nullable
	@Override
	public Session findOne(@NotNull final String id, @NotNull final String userId) throws SQLException {
		@NotNull final String query = "SELECT * FROM app_session WHERE user_id = ? AND id = ?";
		try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
			statement.setString(1, id);
			@NotNull final ResultSet resultSet = statement.executeQuery();
			@Nullable Session session = null;
			if (resultSet.first()) session = transformResult(resultSet);
			resultSet.close();
			return session;
		}
	}

	@NotNull
	@Override
	public List<Session> findAll() throws SQLException {
		@NotNull final String query = "SELECT * FROM app_session";
		try (@NotNull final Statement statement = connection.createStatement();
			 @NotNull final ResultSet resultSet = statement.executeQuery(query)) {
			@NotNull final List<Session> sessions = new ArrayList<>();
			while (resultSet.next()) sessions.add(transformResult(resultSet));
			return sessions;
		}
	}

	@Override
	public void remove(@NotNull final String id) throws SQLException {
		@NotNull final String query = "DELETE FROM app_session WHERE id = ?";
		try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
			statement.setString(1, id);
			statement.executeUpdate();
		}
	}

	@Override
	public void remove(@NotNull final String id, @NotNull final String userId) throws SQLException {
		@NotNull final String query = "DELETE FROM app_session WHERE id = ? AND user_id = ?";
		try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
			statement.setString(1, id);
			statement.setString(2, userId);
			statement.executeUpdate();
		}
	}

	@NotNull
	private Session transformResult(@NotNull final ResultSet resultSet) throws SQLException {
		@NotNull final Session session = new Session();
		session.setId(resultSet.getString("id"));
		session.setUserId(resultSet.getString("user_id"));
		session.setSignature(resultSet.getString("signature"));
		session.setRole(Role.valueOf(resultSet.getString("role")));
		session.setTimestamp(resultSet.getLong("timestamp"));
		return session;
	}

}
