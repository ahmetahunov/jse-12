package ru.ahmetahunov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.repository.IPropertyRepository;
import ru.ahmetahunov.tm.entity.Property;
import ru.ahmetahunov.tm.exception.IdCollisionException;
import java.util.HashMap;
import java.util.Map;

public class PropertyRepository implements IPropertyRepository {

	@NotNull
	protected final Map<String, Property> collection = new HashMap<>();

	@Override
	public void persist(@NotNull final Property property) throws IdCollisionException {
		if (collection.containsKey(property.getName())) throw new IdCollisionException();
		collection.put(property.getName(), property);
	}

	@Override
	public void merge(@NotNull final Property property) {
		collection.put(property.getName(), property);
	}

	@Nullable
	@Override
	public Property findOne(@NotNull final String propertyName) {
		return collection.get(propertyName);
	}

	@Nullable
	@Override
	public Property remove(@NotNull final String propertyName) {
		return collection.remove(propertyName);
	}

	@Override
	public void clear() {
		collection.clear();
	}

}
