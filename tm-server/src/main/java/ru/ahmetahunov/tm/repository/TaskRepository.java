package ru.ahmetahunov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.repository.ITaskRepository;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.enumerated.Status;
import ru.ahmetahunov.tm.util.DateUtil;
import java.sql.*;
import java.util.*;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    public Task persist(@NotNull final Task task) throws SQLException {
        @NotNull final String query = "INSERT INTO app_task VALUES(?, ?, ?, ?, ?, NOW(), ?, ?, ?)";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, task.getId());
            statement.setString(2, task.getName());
            statement.setString(3, task.getDescription());
            statement.setDate(4, DateUtil.parseDate(task.getStartDate()));
            statement.setDate(5, DateUtil.parseDate(task.getFinishDate()));
            statement.setString(6, task.getStatus().toString());
            statement.setString(7, task.getProjectId());
            statement.setString(8, task.getUserId());
            statement.executeUpdate();
            return task;
        }
    }

    @NotNull
    @Override
    public Task merge(@NotNull final Task task) throws SQLException {
        @NotNull final String query = "UPDATE app_task SET name = ?, description = ?, startDate = ?," +
                " finishDate = ?, status = ?, project_id = ? WHERE user_id = ? AND id = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, task.getName());
            statement.setString(2, task.getDescription());
            statement.setDate(3, DateUtil.parseDate(task.getStartDate()));
            statement.setDate(4, DateUtil.parseDate(task.getFinishDate()));
            statement.setString(5, task.getStatus().toString());
            statement.setString(6, task.getProjectId());
            statement.setString(7, task.getUserId());
            statement.setString(8, task.getId());
            statement.executeUpdate();
            return task;
        }
    }

    @Nullable
    @Override
    public Task findOne(@NotNull final String id) throws SQLException {
        @NotNull final String query = "SELECT * FROM app_task WHERE id = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, id);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            @Nullable Task task = null;
            if (resultSet.first()) task = transformResult(resultSet);
            resultSet.close();
            return task;
        }
    }

    @Nullable
    @Override
    public Task findOne(
            @NotNull final String userId,
            @NotNull final String taskId
    ) throws SQLException {
        @NotNull final String query = "SELECT * FROM app_task WHERE user_id = ? AND id = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, userId);
            statement.setString(2, taskId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            @Nullable Task task = null;
            if (resultSet.first()) task = transformResult(resultSet);
            resultSet.close();
            return task;
        }
    }

    @NotNull
    @Override
    public List<Task> findAll() throws SQLException {
        @NotNull final String query = "SELECT * FROM app_task";
        try (@NotNull final Statement statement = connection.createStatement();
             @NotNull final ResultSet resultSet = statement.executeQuery(query)) {
            @NotNull final List<Task> tasks = new ArrayList<>();
            while (resultSet.next()) tasks.add(transformResult(resultSet));
            return tasks;
        }
    }

    @NotNull
    @Override
    public List<Task> findAll(@NotNull final String userId) throws SQLException {
        @NotNull final String query = "SELECT * FROM app_task WHERE user_id = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, userId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            @NotNull final List<Task> tasks = new ArrayList<>();
            while (resultSet.next()) tasks.add(transformResult(resultSet));
            resultSet.close();
            return tasks;
        }
    }

    @NotNull
    @Override
    public List<Task> findAll(
            @NotNull final String userId,
            @NotNull final String comparator
    ) throws SQLException {
        @NotNull final String query = "SELECT * FROM app_task WHERE user_id = ? ORDER BY " + comparator;
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, userId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            @NotNull final List<Task> tasks = new ArrayList<>();
            while (resultSet.next()) tasks.add(transformResult(resultSet));
            resultSet.close();
            return tasks;
        }
    }

    @NotNull
    @Override
    public List<Task> findAll(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String comparator
    ) throws SQLException {
        @NotNull final String query = "SELECT * FROM app_task WHERE user_id = ? AND project_id = ? ORDER BY " + comparator;
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, userId);
            statement.setString(2, projectId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            @NotNull final List<Task> tasks = new ArrayList<>();
            while (resultSet.next()) tasks.add(transformResult(resultSet));
            resultSet.close();
            return tasks;
        }
    }

    @NotNull
    @Override
    public List<Task> findByName(
            @NotNull final String userId,
            @NotNull final String taskName
    ) throws SQLException {
        @NotNull final String query = "SELECT * FROM app_task WHERE user_id = ? AND name LIKE ? ORDER BY name";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, userId);
            statement.setString(2, "%" + taskName + "%");
            @NotNull final ResultSet resultSet = statement.executeQuery();
            @NotNull final List<Task> tasks = new ArrayList<>();
            while (resultSet.next()) tasks.add(transformResult(resultSet));
            resultSet.close();
            return tasks;
        }
    }

    @NotNull
    @Override
    public List<Task> findByDescription(
            @NotNull final String userId,
            @NotNull final String description
    ) throws SQLException {
        @NotNull final String query = "SELECT * FROM app_task WHERE user_id = ? " +
                "AND description LIKE ? ORDER BY description";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, userId);
            statement.setString(2, "%" + description + "%");
            @NotNull final ResultSet resultSet = statement.executeQuery();
            @NotNull final List<Task> tasks = new ArrayList<>();
            while (resultSet.next()) tasks.add(transformResult(resultSet));
            resultSet.close();
            return tasks;
        }
    }

    @NotNull
    @Override
    public List<Task> findByNameOrDesc(
            @NotNull final String userId,
            @NotNull final String searchPhrase
    ) throws SQLException {
        @NotNull final String query = "SELECT * FROM app_task WHERE user_id = ? AND name LIKE ? " +
                "OR description LIKE ? ORDER BY name";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, userId);
            statement.setString(2, "%" + searchPhrase + "%");
            statement.setString(3, "%" + searchPhrase + "%");
            @NotNull final ResultSet resultSet = statement.executeQuery();
            @NotNull final List<Task> tasks = new ArrayList<>();
            while (resultSet.next()) tasks.add(transformResult(resultSet));
            resultSet.close();
            return tasks;
        }
    }

    @Override
    public void remove(@NotNull final String id) throws SQLException {
        @NotNull final String query = "DELETE FROM app_task WHERE id = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, id);
            statement.executeUpdate();
        }
    }

    @Override
    public void remove(
            @NotNull final String userId,
            @NotNull final String taskId
    ) throws SQLException {
        @NotNull final String query = "DELETE FROM app_task WHERE user_id = ? AND id = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, userId);
            statement.setString(2, taskId);
            statement.executeUpdate();
        }
    }

    @Override
    public void removeAll(@NotNull final String userId) throws SQLException {
        @NotNull final String query = "DELETE FROM app_task WHERE user_id = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, userId);
            statement.executeUpdate();
        }
    }

    @Override
    public void load(@NotNull final Collection<Task> data) throws SQLException {
        @NotNull final String query = "INSERT INTO app_task VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            for (@NotNull final Task task : data) {
                statement.setString(1, task.getId());
                statement.setString(2, task.getName());
                statement.setString(3, task.getDescription());
                statement.setDate(4, DateUtil.parseDate(task.getStartDate()));
                statement.setDate(5, DateUtil.parseDate(task.getFinishDate()));
                statement.setTimestamp(6, DateUtil.getTimestamp(task.getCreationDate()));
                statement.setString(7, task.getStatus().toString());
                statement.setString(8, task.getProjectId());
                statement.setString(9, task.getUserId());
                statement.addBatch();
            }
            statement.executeBatch();
            statement.clearBatch();
        }
    }

    @NotNull
    private Task transformResult(@NotNull final ResultSet resultSet) throws SQLException {
        @NotNull final Task task = new Task();
        task.setId(resultSet.getString("id"));
        task.setName(resultSet.getString("name"));
        task.setDescription(resultSet.getString("description"));
        task.setStatus(Status.valueOf(resultSet.getString("status")));
        task.setCreationDate(resultSet.getDate("creationDate"));
        task.setStartDate(resultSet.getDate("startDate"));
        task.setFinishDate(resultSet.getDate("finishDate"));
        task.setUserId(resultSet.getString("user_id"));
        task.setProjectId(resultSet.getString("project_id"));
        return task;
    }

}
