package ru.ahmetahunov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.repository.IUserRepository;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    public User persist(@NotNull final User user) throws SQLException {
        @NotNull final String query = "INSERT INTO app_user VALUES (?, ?, ?, ?)";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, user.getId());
            statement.setString(2, user.getLogin());
            statement.setString(3, user.getPassword());
            statement.setString(4, user.getRole().toString());
            statement.executeUpdate();
            return user;
        }
    }

    @NotNull
    @Override
    public User merge(@NotNull final User user) throws SQLException {
        @NotNull final String query = "UPDATE app_user SET login = ?, password = ?, role = ? WHERE id = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getPassword());
            statement.setString(3, user.getRole().toString());
            statement.setString(4, user.getId());
            statement.executeUpdate();
            return user;
        }
    }

    @Nullable
    @Override
    public User findOne(@NotNull final String id) throws SQLException {
        @NotNull final String query = "SELECT * FROM app_user WHERE id = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, id);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            @Nullable User user = null;
            if (resultSet.first()) user = transformResult(resultSet);
            return user;
        }
    }

    @Nullable
    @Override
    public User findUser(@NotNull final String login) throws SQLException {
        @NotNull final String query = "SELECT * FROM app_user WHERE login = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, login);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            @Nullable User user = null;
            if (resultSet.first()) user = transformResult(resultSet);
            return user;
        }
    }

    @NotNull
    @Override
    public List<User> findAll() throws SQLException {
        @NotNull final String query = "SELECT * FROM app_user";
        try (@NotNull final Statement statement = connection.createStatement();
             @NotNull final ResultSet resultSet = statement.executeQuery(query)) {
            @Nullable List<User> users = new ArrayList<>();
            while (resultSet.next()) users.add(transformResult(resultSet));
            return users;
        }
    }

    @Override
    public void remove(@NotNull final String id) throws SQLException {
        @NotNull final String query = "DELETE FROM app_user WHERE id = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, id);
            statement.executeUpdate();
        }
    }

    @Override
    public void removeAll() throws SQLException {
        @NotNull final String query = "DELETE FROM app_user";
        try (@NotNull final Statement statement = connection.createStatement()) {
            statement.executeQuery(query);
        }
    }

    @Override
    public void load(@NotNull final Collection<User> data) throws SQLException {
        @NotNull final String query = "INSERT IGNORE INTO app_user VALUES (?, ?, ?, ?)";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            for (@NotNull final User user : data) {
                statement.setString(1, user.getId());
                statement.setString(2, user.getLogin());
                statement.setString(3, user.getPassword());
                statement.setString(4, user.getRole().toString());
                statement.addBatch();
            }
            statement.executeBatch();
            statement.clearBatch();
        }
    }

    @Override
    public boolean contains(@NotNull final String login) throws SQLException {
        @NotNull final String query = "SELECT * FROM app_user WHERE login = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, login);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            boolean answer = false;
            if (resultSet.first()) answer = true;
            resultSet.close();
            return answer;
        }
    }


    @NotNull
    private User transformResult(@NotNull final ResultSet resultSet) throws SQLException {
        @NotNull final User user = new User();
        user.setId(resultSet.getString("id"));
        user.setLogin(resultSet.getString("login"));
        user.setPassword(resultSet.getString("password"));
        user.setRole(Role.valueOf(resultSet.getString("role")));
        return user;
    }

}
