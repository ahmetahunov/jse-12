package ru.ahmetahunov.tm.context;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.endpoint.*;
import ru.ahmetahunov.tm.api.repository.*;
import ru.ahmetahunov.tm.api.service.*;
import ru.ahmetahunov.tm.constant.AppConst;
import ru.ahmetahunov.tm.endpoint.*;
import ru.ahmetahunov.tm.entity.Property;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.exception.AccessForbiddenException;
import ru.ahmetahunov.tm.exception.IdCollisionException;
import ru.ahmetahunov.tm.exception.InterruptOperationException;
import ru.ahmetahunov.tm.repository.*;
import ru.ahmetahunov.tm.service.*;
import ru.ahmetahunov.tm.util.PassUtil;
import javax.xml.ws.Endpoint;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.Set;

@NoArgsConstructor
public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final IPropertyRepository propertyRepository = new PropertyRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService(propertyRepository);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(propertyService);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(propertyService);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(propertyService);

    @Getter
    @NotNull
    private final IDataService dataService = new DataService(this);

    @Getter
    @NotNull
    private final ISessionService sessionService = new SessionService(propertyService);

    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpointImpl(this);

    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpointImpl(this);

    @NotNull
    private final UserEndpoint userEndpoint = new UserEndpointImpl(this);

    @NotNull
    private final SessionEndpoint sessionEndpoint = new SessionEndpointImpl(this);

    @NotNull
    private final AdminEndpoint adminEndpoint = new AdminEndpointImpl(this);

    public void init() throws Exception {
        loadProperties();
        prepareDB();
        createDefaultUsers();
        startServer();
    }

    private void loadProperties() throws IOException, IdCollisionException, InterruptOperationException {
        @NotNull final Properties properties = new Properties();
        @NotNull final ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        @Nullable final InputStream fis = classLoader.getResourceAsStream("application.properties");
        properties.load(fis);
        @NotNull final Set<String> names = properties.stringPropertyNames();
        for (@NotNull final String name : names) {
            @NotNull final Property property = new Property();
            property.setName(name);
            property.setValue(properties.getProperty(name));
            propertyService.persist(property);
        }
    }

    private void prepareDB() throws Exception {
        @NotNull final String url = propertyService.findOne(AppConst.DB_START_HOST).getValue();
        @NotNull final String userName = propertyService.findOne(AppConst.DB_LOGIN).getValue();
        @NotNull final String password = propertyService.findOne(AppConst.DB_PASSWORD).getValue();
        Class.forName(propertyService.findOne(AppConst.DB_DRIVER).getValue());
        @NotNull final Connection connection = DriverManager.getConnection(url, userName, password);
        @Nullable final Statement st = connection.createStatement();
        if (st == null) throw new InterruptOperationException("Prepare db failed.");
        st.execute(propertyService.findOne(AppConst.DB_CREATE).getValue());
        st.execute(propertyService.findOne(AppConst.DB_USE).getValue());
        st.execute(propertyService.findOne(AppConst.USER_TABLE).getValue());
        st.execute(propertyService.findOne(AppConst.SESSION_TABLE).getValue());
        st.execute(propertyService.findOne(AppConst.PROJECT_TABLE).getValue());
        st.execute(propertyService.findOne(AppConst.TASK_TABLE).getValue());
        st.close();
        System.out.println("Database ready");
    }

    private void createDefaultUsers() throws AccessForbiddenException, InterruptOperationException, SQLException {
        if (!userService.contains("user")) {
            @NotNull final User user = new User();
            user.setId("64a8bae7-e916-4d1d-9b1e-04bc19d0bd91");
            user.setLogin(propertyService.findOne(AppConst.USER_LOGIN).getValue());
            user.setPassword(PassUtil.getHash(propertyService.findOne(AppConst.USER_PASSWORD).getValue()));
            userService.persist(user);
        }
        if (!userService.contains("admin")) {
            @NotNull final User admin = new User();
            admin.setId("3d8043c1-6aeb-4df9-af8d-7a8de0f99a09");
            admin.setLogin(propertyService.findOne(AppConst.ADMIN_LOGIN).getValue());
            admin.setPassword(PassUtil.getHash(propertyService.findOne(AppConst.ADMIN_PASSWORD).getValue()));
            admin.setRole(Role.ADMINISTRATOR);
            userService.persist(admin);
        }
    }

    private void startServer() {
        Endpoint.publish(AppConst.USER_ENDPOINT, userEndpoint);
        System.out.println(AppConst.USER_ENDPOINT + " is running...");
        Endpoint.publish(AppConst.SESSION_ENDPOINT, sessionEndpoint);
        System.out.println(AppConst.SESSION_ENDPOINT + " is running...");
        Endpoint.publish(AppConst.TASK_ENDPOINT, taskEndpoint);
        System.out.println(AppConst.TASK_ENDPOINT + " is running...");
        Endpoint.publish(AppConst.PROJECT_ENDPOINT, projectEndpoint);
        System.out.println(AppConst.PROJECT_ENDPOINT + " is running...");
        Endpoint.publish(AppConst.ADMIN_ENDPOINT, adminEndpoint);
        System.out.println(AppConst.ADMIN_ENDPOINT + " is running...");
    }

}
