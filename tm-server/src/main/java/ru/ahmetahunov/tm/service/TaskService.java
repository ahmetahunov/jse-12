package ru.ahmetahunov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.repository.ITaskRepository;
import ru.ahmetahunov.tm.api.service.IPropertyService;
import ru.ahmetahunov.tm.api.service.ITaskService;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.exception.InterruptOperationException;
import ru.ahmetahunov.tm.repository.TaskRepository;
import ru.ahmetahunov.tm.util.ComparatorUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public final class TaskService extends AbstractService<Task> implements ITaskService {

    public TaskService(@NotNull final IPropertyService propertyService) {
        super(propertyService);
    }

    @Nullable
    @Override
    public Task persist(@Nullable final Task task) throws InterruptOperationException {
        if (task == null) return null;
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final ITaskRepository repository = new TaskRepository(connection);
            return repository.persist(task);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new InterruptOperationException("Operation failed!");
        }
    }

    @Nullable
    @Override
    public Task merge(@Nullable final Task task) throws InterruptOperationException {
        if (task == null) return null;
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final ITaskRepository repository = new TaskRepository(connection);
            return repository.merge(task);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new InterruptOperationException("Operation failed!");
        }
    }

    @Nullable
    @Override
    public Task findOne(@Nullable final String id) throws InterruptOperationException, SQLException {
        if (id == null || id.isEmpty()) return null;
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final ITaskRepository repository = new TaskRepository(connection);
            return repository.findOne(id);
        }
    }

    @Nullable
    @Override
    public Task findOne(
            @Nullable final String userId,
            @Nullable final String taskId
    ) throws InterruptOperationException, SQLException {
        if (userId == null || userId.isEmpty()) return null;
        if (taskId == null || taskId.isEmpty()) return null;
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final ITaskRepository repository = new TaskRepository(connection);
            return repository.findOne(userId, taskId);
        }
    }

    @NotNull
    @Override
    public List<Task> findAll() throws InterruptOperationException, SQLException {
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final ITaskRepository repository = new TaskRepository(connection);
            return repository.findAll();
        }
    }

    @NotNull
    @Override
    public List<Task> findAll(@Nullable final String userId) throws InterruptOperationException, SQLException {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final ITaskRepository repository = new TaskRepository(connection);
            return repository.findAll(userId);
        }
    }

    @NotNull
    @Override
    public List<Task> findAll(
            @Nullable final String userId,
            @Nullable String comparator
    ) throws InterruptOperationException, SQLException {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final ITaskRepository repository = new TaskRepository(connection);
            return repository.findAll(userId, ComparatorUtil.getComparator(comparator));
        }
    }

    @NotNull
    @Override
    public List<Task> findAll(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable String comparator
    ) throws InterruptOperationException, SQLException {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final ITaskRepository repository = new TaskRepository(connection);
            return repository.findAll(userId, projectId, ComparatorUtil.getComparator(comparator));
        }
    }

    @NotNull
    @Override
    public List<Task> findByName(
            @Nullable final String userId,
            @Nullable final String taskName
    ) throws InterruptOperationException, SQLException {
        if (taskName == null || taskName.isEmpty()) return Collections.emptyList();
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final ITaskRepository repository = new TaskRepository(connection);
            return repository.findByName(userId, taskName);
        }
    }

    @NotNull
    @Override
    public List<Task> findByDescription(
            @Nullable final String userId,
            @Nullable final String description
    ) throws InterruptOperationException, SQLException {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (description == null) return Collections.emptyList();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final ITaskRepository repository = new TaskRepository(connection);
            return repository.findByDescription(userId, description);
        }
    }

    @NotNull
    @Override
    public List<Task> findByNameOrDesc(
            @Nullable final String userId,
            @Nullable final String searchPhrase
    ) throws InterruptOperationException, SQLException {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (searchPhrase == null || searchPhrase.isEmpty()) return Collections.emptyList();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final ITaskRepository repository = new TaskRepository(connection);
            return repository.findByNameOrDesc(userId, searchPhrase);
        }
    }

    @Override
    public void remove(
            @Nullable final String userId,
            @Nullable final String taskId
    ) throws SQLException, InterruptOperationException {
        if (taskId == null || taskId.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull final Savepoint point = connection.setSavepoint("point");
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        try {
            repository.remove(userId, taskId);
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            connection.rollback(point);
            throw new InterruptOperationException("Operation failed!");
        } finally {
            connection.releaseSavepoint(point);
            connection.close();
        }
    }

    @Override
    public void remove(@Nullable final String id) throws InterruptOperationException, SQLException {
        if (id == null || id.isEmpty()) return;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull final Savepoint point = connection.setSavepoint("point");
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        try {
            repository.remove(id);
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            connection.rollback(point);
            throw new InterruptOperationException("Operation failed!");
        } finally {
            connection.releaseSavepoint(point);
            connection.close();
        }
    }

    @Override
    public void remove(@Nullable final Task task) throws InterruptOperationException, SQLException {
        if (task == null) return;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull final Savepoint point = connection.setSavepoint("point");
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        try {
            repository.remove(task.getUserId(), task.getId());
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            connection.rollback(point);
            throw new InterruptOperationException("Operation failed!");
        } finally {
            connection.releaseSavepoint(point);
            connection.close();
        }
    }

    @Override
    public void removeAll(@Nullable final String userId) throws InterruptOperationException, SQLException {
        if (userId == null || userId.isEmpty()) return;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull final Savepoint point = connection.setSavepoint("point");
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        try {
            repository.removeAll(userId);
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            connection.rollback(point);
            throw new InterruptOperationException("Operation failed!");
        } finally {
            connection.releaseSavepoint(point);
            connection.close();
        }
    }

    @Override
    public void load(@Nullable final Collection<Task> data) throws InterruptOperationException, SQLException {
        if (data == null) return;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull final Savepoint point = connection.setSavepoint("point");
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        try {
            repository.load(data);
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            connection.rollback(point);
            throw new InterruptOperationException("Operation failed!");
        } finally {
            connection.releaseSavepoint(point);
            connection.close();
        }
    }

}
