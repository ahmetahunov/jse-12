package ru.ahmetahunov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.repository.ISessionRepository;
import ru.ahmetahunov.tm.api.service.IPropertyService;
import ru.ahmetahunov.tm.api.service.ISessionService;
import ru.ahmetahunov.tm.entity.Session;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.exception.AccessForbiddenException;
import ru.ahmetahunov.tm.exception.InterruptOperationException;
import ru.ahmetahunov.tm.repository.SessionRepository;
import ru.ahmetahunov.tm.util.SessionSignatureUtil;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.util.List;

public final class SessionService extends AbstractService<Session> implements ISessionService {

	public SessionService(@NotNull final IPropertyService propertyService) {
		super(propertyService);
	}

	@Nullable
	@Override
	public Session persist(@Nullable final Session session) throws InterruptOperationException {
		if (session == null) return null;
		try (@NotNull final Connection connection = getConnection()) {
			@NotNull final ISessionRepository repository = new SessionRepository(connection);
			return repository.persist(session);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new InterruptOperationException("Operation failed!");
		}
	}

	@Nullable
	@Override
	public Session merge(@Nullable final Session session) throws InterruptOperationException {
		if (session == null) return null;
		try (@NotNull final Connection connection = getConnection()) {
			@NotNull final ISessionRepository repository = new SessionRepository(connection);
			return repository.merge(session);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new InterruptOperationException("Operation failed!");
		}
	}

	@Nullable
	@Override
	public Session findOne(
			@Nullable final String id,
			@Nullable final String userId
	) throws InterruptOperationException, SQLException {
		if (id == null || id.isEmpty()) return null;
		if (userId == null || userId.isEmpty()) return null;
		try (@NotNull final Connection connection = getConnection()) {
			@NotNull final ISessionRepository repository = new SessionRepository(connection);
			return repository.findOne(id, userId);
		}
	}

	@Nullable
	@Override
	public Session findOne(@Nullable final String id) throws InterruptOperationException, SQLException {
		if (id == null || id.isEmpty()) return null;
		try (@NotNull final Connection connection = getConnection()) {
			@NotNull final ISessionRepository repository = new SessionRepository(connection);
			return repository.findOne(id);
		}
	}

	@Override
	public @NotNull List<Session> findAll() throws InterruptOperationException, SQLException {
		try (@NotNull final Connection connection = getConnection()) {
			@NotNull final ISessionRepository repository = new SessionRepository(connection);
			return repository.findAll();
		}
	}

	@Override
	public void remove(
			@Nullable final String id,
			@Nullable final String userId
	) throws InterruptOperationException, SQLException {
		if (id == null || id.isEmpty()) return;
		if (userId == null || userId.isEmpty()) return;
		@NotNull final Connection connection = getConnection();
		connection.setAutoCommit(false);
		@NotNull final Savepoint point = connection.setSavepoint("point");
		@NotNull final ISessionRepository repository = new SessionRepository(connection);
		try {
			repository.remove(id, userId);
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			connection.rollback(point);
		} finally {
			connection.releaseSavepoint(point);
			connection.close();
		}
	}

	@Override
	public void remove(@Nullable final String id) throws SQLException, InterruptOperationException {
		if (id == null || id.isEmpty()) return;
		@NotNull final Connection connection = getConnection();
		connection.setAutoCommit(false);
		@NotNull final Savepoint point = connection.setSavepoint("point");
		@NotNull final ISessionRepository repository = new SessionRepository(connection);
		try {
			repository.remove(id);
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			connection.rollback(point);
		} finally {
			connection.releaseSavepoint(point);
			connection.close();
		}
	}

	@Override
	public void remove(@Nullable final Session session) throws InterruptOperationException, SQLException {
		if (session == null) return;
		@NotNull final Connection connection = getConnection();
		connection.setAutoCommit(false);
		@NotNull final Savepoint point = connection.setSavepoint("point");
		@NotNull final ISessionRepository repository = new SessionRepository(connection);
		try {
			repository.remove(session.getId());
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			connection.rollback(point);
		} finally {
			connection.releaseSavepoint(point);
			connection.close();
		}
	}

	@Override
	public void validate(@Nullable final Session session)
			throws AccessForbiddenException, InterruptOperationException, SQLException {
		if (session == null) throw new AccessForbiddenException("Access denied!");
		final boolean isUserIdNull = session.getUserId() == null;
		final boolean isSignatureNull = session.getSignature() == null;
		final boolean isRoleNull = session.getRole() == null;
		if (isUserIdNull || isSignatureNull || isRoleNull)
			throw new AccessForbiddenException("Access denied!");
		try (@NotNull final Connection connection = getConnection()) {
			@NotNull final ISessionRepository repository = new SessionRepository(connection);
			@Nullable final Session checkSession = repository.findOne(session.getId());
			if (checkSession == null || checkSession.getSignature() == null)
				throw new AccessForbiddenException("Access denied!");
			@Nullable final String signature = session.getSignature();
			session.setSignature(null);
			@Nullable final String hash = SessionSignatureUtil.sign(session);
			if (!checkSession.getSignature().equals(hash) || !signature.equals(hash))
				throw new AccessForbiddenException("Access denied!");
			final long existTime = System.currentTimeMillis() - session.getTimestamp();
			if (existTime > 32400000) {
				repository.remove(session.getId());
				throw new AccessForbiddenException("Access denied! Session has expired.");
			}
		}
	}

	@Override
	public void validate(
			@Nullable final Session session,
			@NotNull final Role role
	) throws AccessForbiddenException, SQLException, InterruptOperationException {
		validate(session);
		if (!role.equals(session.getRole())) throw new AccessForbiddenException("Access denied!");
	}

}
