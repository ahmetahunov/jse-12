package ru.ahmetahunov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.repository.IUserRepository;
import ru.ahmetahunov.tm.api.service.IPropertyService;
import ru.ahmetahunov.tm.api.service.IUserService;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.exception.AccessForbiddenException;
import ru.ahmetahunov.tm.exception.InterruptOperationException;
import ru.ahmetahunov.tm.repository.UserRepository;
import ru.ahmetahunov.tm.util.PassUtil;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.util.Collection;
import java.util.List;

public final class UserService extends AbstractService<User> implements IUserService {

    public UserService(@NotNull final IPropertyService propertyService) {
        super(propertyService);
    }

    @Nullable
    @Override
    public User persist(@Nullable final User user) throws InterruptOperationException {
        if (user == null) return null;
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserRepository repository = new UserRepository(connection);
            return repository.persist(user);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new InterruptOperationException("Operation failed!");
        }
    }

    @Nullable
    @Override
    public User merge(@Nullable final User user) throws InterruptOperationException {
        if (user == null) return null;
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserRepository repository = new UserRepository(connection);
            return repository.merge(user);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new InterruptOperationException("Operation failed!");
        }
    }

    @Nullable
    @Override
    public User findUser(@Nullable final String login) throws InterruptOperationException, SQLException {
        if (login == null || login.isEmpty()) return null;
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserRepository repository = new UserRepository(connection);
            return repository.findUser(login);
        }
    }

    @Nullable
    @Override
    public User findOne(@Nullable final String id) throws InterruptOperationException, SQLException {
        if (id == null || id.isEmpty()) return null;
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserRepository repository = new UserRepository(connection);
            return repository.findOne(id);
        }
    }

    @NotNull
    @Override
    public List<User> findAll() throws InterruptOperationException, SQLException {
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserRepository repository = new UserRepository(connection);
            return repository.findAll();
        }
    }

    @Override
    public void updatePasswordAdmin(@Nullable final String userId, @Nullable final String password)
            throws InterruptOperationException, AccessForbiddenException {
        if (userId == null || userId.isEmpty())
            throw new InterruptOperationException("This user does not exist.");
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserRepository repository = new UserRepository(connection);
            @Nullable final User user = repository.findOne(userId);
            if (user == null) throw new InterruptOperationException("This user does not exist.");
            @NotNull final String hash = PassUtil.getHash(password);
            user.setPassword(hash);
            repository.merge(user);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new InterruptOperationException("Operation failed!");
        }
    }

    @Override
    public void updateRole(@Nullable final String userId, @Nullable final Role role)
            throws InterruptOperationException {
        if (userId == null || userId.isEmpty())
            throw new InterruptOperationException("This user does not exist.");
        if (role == null) throw new InterruptOperationException("Unknown role.");
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserRepository repository = new UserRepository(connection);
            @Nullable final User user = repository.findOne(userId);
            if (user == null) throw new InterruptOperationException("This user does not exist.");
            user.setRole(role);
            repository.merge(user);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new InterruptOperationException("Operation failed!");
        }
    }

    @Override
    public void updatePassword(
            @Nullable final String userId,
            @Nullable final String oldPassword,
            @Nullable final String password
    ) throws AccessForbiddenException, InterruptOperationException, SQLException {
        if (userId == null || userId.isEmpty())
            throw new AccessForbiddenException("This user does not exist.");
        if (oldPassword == null || oldPassword.isEmpty())
            throw new AccessForbiddenException("Wrong password.");
        if (password == null || password.isEmpty())
            throw new AccessForbiddenException("Wrong password.");
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserRepository repository = new UserRepository(connection);
            @Nullable final User user = repository.findOne(userId);
            if (user == null) throw new InterruptOperationException("This user does not exist.");
            @NotNull final String hash = PassUtil.getHash(oldPassword);
            if (!user.getPassword().equals(hash)) throw new AccessForbiddenException("Wrong password.");
            @NotNull final String newHash = PassUtil.getHash(password);
            user.setPassword(newHash);
            repository.merge(user);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new InterruptOperationException("Operation failed!");
        }
    }

    @Override
    public void updateLogin(
            @Nullable final String userId,
            @Nullable final String login
    ) throws AccessForbiddenException, InterruptOperationException {
        if (login == null || login.isEmpty()) throw new InterruptOperationException("Wrong format login.");
        if (userId == null || userId.isEmpty()) throw new AccessForbiddenException("This user does not exist.");
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserRepository repository = new UserRepository(connection);
            @Nullable final User user = repository.findOne(userId);
            if (user == null) throw new InterruptOperationException("This user does not exist.");
            user.setLogin(login);
            repository.merge(user);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new InterruptOperationException("Operation failed!");
        }
    }

    @Override
    public void remove(@Nullable final String id) throws InterruptOperationException, SQLException {
        if (id == null || id.isEmpty()) return;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull final Savepoint point = connection.setSavepoint("point");
        @NotNull final IUserRepository repository = new UserRepository(connection);
        try {
            repository.remove(id);
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            connection.rollback(point);
            throw new InterruptOperationException("Operation failed!");
        } finally {
            connection.releaseSavepoint(point);
            connection.close();
        }
    }

    @Override
    public void remove(@Nullable final User user) throws InterruptOperationException, SQLException {
        if (user == null) return;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull final Savepoint point = connection.setSavepoint("point");
        @NotNull final IUserRepository repository = new UserRepository(connection);
        try {
            repository.remove(user.getId());
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            connection.rollback(point);
            throw new InterruptOperationException("Operation failed!");
        } finally {
            connection.releaseSavepoint(point);
            connection.close();
        }
    }

    @Override
    public void load(@Nullable final Collection<User> data) throws InterruptOperationException, SQLException {
        if (data == null) return;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull final Savepoint point = connection.setSavepoint("point");
        @NotNull final IUserRepository repository = new UserRepository(connection);
        try {
            repository.load(data);
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            connection.rollback(point);
            throw new InterruptOperationException("Operation failed!");
        } finally {
            connection.releaseSavepoint(point);
            connection.close();
        }
    }

    @Override
    public boolean contains(@Nullable final String login) throws SQLException, InterruptOperationException {
        if (login == null || login.isEmpty()) return true;
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserRepository repository = new UserRepository(connection);
            return repository.contains(login);
        }
    }

}
