package ru.ahmetahunov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.repository.IProjectRepository;
import ru.ahmetahunov.tm.api.service.IProjectService;
import ru.ahmetahunov.tm.api.service.IPropertyService;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.exception.InterruptOperationException;
import ru.ahmetahunov.tm.repository.ProjectRepository;
import ru.ahmetahunov.tm.util.ComparatorUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public final class ProjectService extends AbstractService<Project> implements IProjectService {

    public ProjectService(@NotNull final IPropertyService propertyService) {
        super(propertyService);
    }

    @Nullable
    @Override
    public Project persist(@Nullable final Project project) throws InterruptOperationException {
        if (project == null) return null;
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IProjectRepository repository = new ProjectRepository(connection);
            return repository.persist(project);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new InterruptOperationException("Operation failed!");
        }
    }

    @Nullable
    @Override
    public Project merge(@Nullable final Project project) throws InterruptOperationException {
        if (project == null) return null;
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IProjectRepository repository = new ProjectRepository(connection);
            return repository.merge(project);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new InterruptOperationException("Operation failed!");
        }
    }

    @Nullable
    @Override
    public Project findOne(
            @Nullable final String userId,
            @Nullable final String id
    ) throws SQLException, InterruptOperationException {
        if (id == null || id.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
       try (@NotNull final Connection connection = getConnection()) {
           @NotNull final IProjectRepository repository = new ProjectRepository(connection);
           return repository.findOne(userId, id);
       }
    }

    @Nullable
    @Override
    public Project findOne(@Nullable final String id) throws InterruptOperationException, SQLException {
        if (id == null || id.isEmpty()) return null;
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IProjectRepository repository = new ProjectRepository(connection);
            return repository.findOne(id);
        }
    }

    @Override
    public @NotNull List<Project> findAll() throws InterruptOperationException, SQLException {
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IProjectRepository repository = new ProjectRepository(connection);
            return repository.findAll();
        }
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String userId) throws InterruptOperationException, SQLException {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IProjectRepository repository = new ProjectRepository(connection);
            return repository.findAll(userId);
        }
    }

    @NotNull
    @Override
    public List<Project> findAll(
            @Nullable final String userId,
            @Nullable String comparator
    ) throws InterruptOperationException, SQLException {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IProjectRepository repository = new ProjectRepository(connection);
            return repository.findAll(userId, ComparatorUtil.getComparator(comparator));
        }
    }

    @NotNull
    @Override
    public List<Project> findByName(
            @Nullable final String userId,
            @Nullable final String projectName
    ) throws InterruptOperationException, SQLException {
        if (projectName == null || projectName.isEmpty()) return Collections.emptyList();
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IProjectRepository repository = new ProjectRepository(connection);
            return repository.findByName(userId, projectName);
        }
    }

    @NotNull
    @Override
    public List<Project> findByDescription(
            @Nullable final String userId,
            @Nullable final String description
    ) throws InterruptOperationException, SQLException {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (description == null) return Collections.emptyList();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IProjectRepository repository = new ProjectRepository(connection);
            return repository.findByDescription(userId, description);
        }
    }

    @NotNull
    @Override
    public List<Project> findByNameOrDesc(
            @Nullable final String userId,
            @Nullable final String searchPhrase
    ) throws InterruptOperationException, SQLException {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (searchPhrase == null) return Collections.emptyList();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IProjectRepository repository = new ProjectRepository(connection);
            return repository.findByNameOrDesc(userId, searchPhrase);
        }
    }

    @Override
    public void remove(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws InterruptOperationException, SQLException {
        if (userId == null || userId.isEmpty()) return;
        if (projectId == null || projectId.isEmpty()) return;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull final Savepoint point = connection.setSavepoint("point");
        @NotNull final IProjectRepository repository = new ProjectRepository(connection);
        try {
            repository.remove(userId, projectId);
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            connection.rollback(point);
            throw new InterruptOperationException("Operation failed!");
        } finally {
            connection.releaseSavepoint(point);
            connection.close();
        }
    }

    @Override
    public void remove(@Nullable final String id) throws SQLException, InterruptOperationException {
        if (id == null || id.isEmpty()) return;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull final Savepoint point = connection.setSavepoint("point");
        @NotNull final IProjectRepository repository = new ProjectRepository(connection);
        try {
            repository.remove(id);
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            connection.rollback(point);
            throw new InterruptOperationException("Operation failed!");
        } finally {
            connection.releaseSavepoint(point);
            connection.close();
        }
    }

    @Override
    public void remove(@Nullable final Project project) throws SQLException, InterruptOperationException {
        if (project == null) return;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull final Savepoint point = connection.setSavepoint("point");
        @NotNull final IProjectRepository repository = new ProjectRepository(connection);
        try {
            repository.remove(project.getId());
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            connection.rollback(point);
            throw new InterruptOperationException("Operation failed!");
        } finally {
            connection.releaseSavepoint(point);
            connection.close();
        }
    }

    @Override
    public void removeAll(@Nullable final String userId) throws InterruptOperationException, SQLException {
        if (userId == null || userId.isEmpty()) return;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull final Savepoint point = connection.setSavepoint("point");
        @NotNull final IProjectRepository repository = new ProjectRepository(connection);
        try {
            repository.removeAll(userId);
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            connection.rollback(point);
            throw new InterruptOperationException("Operation failed!");
        } finally {
            connection.releaseSavepoint(point);
            connection.close();
        }
    }

    @Override
    public void load(@Nullable final Collection<Project> data) throws InterruptOperationException, SQLException {
        if (data == null) return;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull final Savepoint point = connection.setSavepoint("point");
        @NotNull final IProjectRepository repository = new ProjectRepository(connection);
        try {
            repository.load(data);
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            connection.rollback(point);
            throw new InterruptOperationException("Operation failed!");
        } finally {
            connection.releaseSavepoint(point);
            connection.close();
        }
    }

}
