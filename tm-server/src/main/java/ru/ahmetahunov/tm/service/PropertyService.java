package ru.ahmetahunov.tm.service;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.repository.IPropertyRepository;
import ru.ahmetahunov.tm.api.service.IPropertyService;
import ru.ahmetahunov.tm.entity.Property;
import ru.ahmetahunov.tm.exception.IdCollisionException;
import ru.ahmetahunov.tm.exception.InterruptOperationException;

@RequiredArgsConstructor
public final class PropertyService implements IPropertyService {

	@NotNull
	private final IPropertyRepository repository;

	@Override
	public void persist(@Nullable final Property property) throws IdCollisionException, InterruptOperationException {
		if (property == null) throw new InterruptOperationException("Unknown property.");
		repository.persist(property);
	}

	@Override
	public void merge(@Nullable final Property property) throws InterruptOperationException {
		if (property == null) throw new InterruptOperationException("Unknown property.");
		repository.merge(property);
	}

	@NotNull
	@Override
	public Property findOne(@Nullable final String propertyName) throws InterruptOperationException {
		if (propertyName == null || propertyName.isEmpty()) throw new InterruptOperationException("Unknown property");
		@Nullable final Property property = repository.findOne(propertyName);
		if (property == null) throw new InterruptOperationException("Property does not exist.");
		return property;
	}

	@Nullable
	@Override
	public Property remove(@Nullable final String propertyName) throws InterruptOperationException {
		if (propertyName == null || propertyName.isEmpty()) throw new InterruptOperationException("Unknown property");
		return repository.remove(propertyName);
	}

	@Override
	public void clear() {
		repository.clear();
	}

}
