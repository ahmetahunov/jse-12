package ru.ahmetahunov.tm.service;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.service.IAbstractService;
import ru.ahmetahunov.tm.api.service.IPropertyService;
import ru.ahmetahunov.tm.constant.AppConst;
import ru.ahmetahunov.tm.entity.AbstractEntity;
import ru.ahmetahunov.tm.exception.InterruptOperationException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

@RequiredArgsConstructor
public abstract class AbstractService<T extends AbstractEntity> implements IAbstractService<T> {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    protected Connection getConnection() throws InterruptOperationException {
        try {
            @NotNull final String url = propertyService.findOne(AppConst.DB_HOST).getValue();
            @NotNull final String userName = propertyService.findOne(AppConst.DB_LOGIN).getValue();
            @NotNull final String password = propertyService.findOne(AppConst.DB_PASSWORD).getValue();
            Class.forName(propertyService.findOne(AppConst.DB_DRIVER).getValue());
            return DriverManager.getConnection(url, userName, password);
        } catch (InterruptOperationException | SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            throw new InterruptOperationException("Can't get connection");
        }
    }

    @Nullable
    @Override
    public abstract T persist(@Nullable final T item) throws InterruptOperationException;

    @Nullable
    @Override
    public abstract T merge(@Nullable final T item) throws InterruptOperationException;

    @Nullable
    @Override
    public abstract T findOne(@Nullable final String id) throws InterruptOperationException, SQLException;

    @NotNull
    @Override
    public abstract List<T> findAll() throws InterruptOperationException, SQLException;

    @Override
    public abstract void remove(@Nullable final T item) throws SQLException, InterruptOperationException;

    @Override
    public abstract void remove(@Nullable final String id) throws SQLException, InterruptOperationException;

}
