package ru.ahmetahunov.tm.util;

import org.jetbrains.annotations.Nullable;
import java.sql.Date;
import java.sql.Timestamp;

public final class DateUtil {

	@Nullable
	public static Date parseDate(java.util.Date date) {
		if (date == null) return null;
		return new Date(date.getTime());
	}

	@Nullable
	public static Timestamp getTimestamp(java.util.Date date) {
		if (date == null) return null;
		return new Timestamp(date.getTime());
	}

}
