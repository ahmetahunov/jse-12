package ru.ahmetahunov.tm.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.exception.AccessForbiddenException;
import ru.ahmetahunov.tm.constant.AppConst;

public final class SessionSignatureUtil {

	@Nullable
	public static String sign(@Nullable final Object value) throws AccessForbiddenException {
		try {
			@NotNull final ObjectMapper objectMapper = new ObjectMapper();
			@NotNull final String json = objectMapper.writeValueAsString(value);
			return sign(json);
		} catch (final JsonProcessingException e) {
			return null;
		}
	}

	@Nullable
	public static String sign(@Nullable final String value) throws AccessForbiddenException {
		if (value == null) return null;
		@Nullable String result = value;
		for (int i = 0; i < AppConst.CYCLE; i++) {
			result = PassUtil.getHash(AppConst.SALT + result + AppConst.SALT);
		}
		return result;
	}

}
