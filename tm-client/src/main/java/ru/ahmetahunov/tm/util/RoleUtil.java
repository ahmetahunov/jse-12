package ru.ahmetahunov.tm.util;

import org.jetbrains.annotations.NotNull;
import ru.ahmetahunov.tm.api.endpoint.Role;
import ru.ahmetahunov.tm.exception.FailedOperationException;

public final class RoleUtil {

    @NotNull
    public static Role getRole(@NotNull final String role) throws FailedOperationException {
        if ("user".equals(role.toLowerCase())) return Role.USER;
        if ("administrator".equals(role.toLowerCase())) return Role.ADMINISTRATOR;
        throw new FailedOperationException("Unknown role");
    }

}
