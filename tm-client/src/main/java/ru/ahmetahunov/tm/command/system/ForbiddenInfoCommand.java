package ru.ahmetahunov.tm.command.system;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.endpoint.Role;
import ru.ahmetahunov.tm.command.AbstractCommand;

@NoArgsConstructor
public final class ForbiddenInfoCommand extends AbstractCommand {

    @Override
    public boolean isSecure() {
        return false;
    }

    @NotNull
    @Override
    public String getName() {
        return "forbidden";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Notifies about low level access rights.";
    }

    @Override
    public void execute() {
        serviceLocator.getTerminalService().writeMessage("You don't have permission. Operation rejected");
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[0];
    }

}
