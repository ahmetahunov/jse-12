package ru.ahmetahunov.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.endpoint.*;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.exception.FailedOperationException;
import java.lang.Exception;

@NoArgsConstructor
public final class UserRegistrationAdminCommand extends AbstractCommand {

    @Override
    public boolean isSecure() {
        return false;
    }

    @NotNull
    @Override
    public String getName() {
        return "user-register-admin";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "New user registration with role setting.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @Nullable final Session session = serviceLocator.getStateService().getSession();
        terminalService.writeMessage("[REGISTRATION ADMIN]");
        @NotNull final String login = terminalService.getAnswer("Please enter login: ");
        @NotNull String password = terminalService.getAnswer("Please enter password: ");
        @NotNull final String repeatPass = terminalService.getAnswer("Please enter new password one more time: ");
        if (password.isEmpty() || !password.equals(repeatPass))
            throw new FailedOperationException("Passwords do not match!");
        serviceLocator.getAdminEndpoint().userRegisterAdmin(session, login, password);
        terminalService.writeMessage("[OK]");
    }

    @Nullable
    @Override
    public Role[] getRoles() { return new Role[] { Role.ADMINISTRATOR }; }

}
