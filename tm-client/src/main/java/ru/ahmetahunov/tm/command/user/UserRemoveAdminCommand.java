package ru.ahmetahunov.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.endpoint.Role;
import ru.ahmetahunov.tm.api.endpoint.Session;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.command.AbstractCommand;

@NoArgsConstructor
public final class UserRemoveAdminCommand extends AbstractCommand {

    @Override
    public boolean isSecure() { return false; }

    @NotNull
    @Override
    public String getName() {
        return "user-remove";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove selected user and all his projects and tasks.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @Nullable final Session session = serviceLocator.getStateService().getSession();
        terminalService.writeMessage("[USER REMOVE]");
        @NotNull String userId = terminalService.getAnswer("Please enter user id: ");
        serviceLocator.getAdminEndpoint().userRemove(session, userId);
        terminalService.writeMessage("[OK]");
    }

    @Nullable
    @Override
    public Role[] getRoles() { return new Role[] { Role.ADMINISTRATOR }; }

}
