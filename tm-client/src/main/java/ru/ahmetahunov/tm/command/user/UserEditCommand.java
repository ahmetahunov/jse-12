package ru.ahmetahunov.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.endpoint.*;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import java.lang.Exception;

@NoArgsConstructor
public final class UserEditCommand extends AbstractCommand {

    @Override
    public boolean isSecure() { return false; }

    @NotNull
    @Override
    public String getName() {
        return "user-edit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Edit user's information.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @Nullable final Session session = serviceLocator.getStateService().getSession();
        terminalService.writeMessage("[USER EDIT]");
        @NotNull final String login = terminalService.getAnswer("Please enter new login: ");
        serviceLocator.getUserEndpoint().updateLogin(session, login);
        terminalService.writeMessage("[OK]");
    }

    @Nullable
    @Override
    public Role[] getRoles() { return new Role[] { Role.USER, Role.ADMINISTRATOR }; }

}
